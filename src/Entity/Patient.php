<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PatientRepository")
 */
class Patient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="patient", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $patientid;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dob;

    /**
     * @ORM\Column(type="integer")
     */
    private $age;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_became_patient;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $other_details;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPatientid(): ?User
    {
        return $this->patientid;
    }

    public function setPatientid(User $patientid): self
    {
        $this->patientid = $patientid;

        return $this;
    }

    public function getDob(): ?\DateTimeInterface
    {
        return $this->dob;
    }

    public function setDob(?\DateTimeInterface $dob): self
    {
        $this->dob = $dob;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getDateBecamePatient(): ?\DateTimeInterface
    {
        return $this->date_became_patient;
    }

    public function setDateBecamePatient(\DateTimeInterface $date_became_patient): self
    {
        $this->date_became_patient = $date_became_patient;

        return $this;
    }

    public function getOtherDetails(): ?string
    {
        return $this->other_details;
    }

    public function setOtherDetails(?string $other_details): self
    {
        $this->other_details = $other_details;

        return $this;
    }
}
