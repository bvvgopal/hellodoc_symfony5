<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DistrictRepository")
 */
class District
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $districtname;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\States", inversedBy="districts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $state_id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Location", mappedBy="district_id", orphanRemoval=true)
     */
    private $locations;

    public function __construct()
    {
        $this->locations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDistrictname(): ?string
    {
        return $this->districtname;
    }

    public function setDistrictname(string $districtname): self
    {
        $this->districtname = $districtname;

        return $this;
    }

    public function getStateId(): ?States
    {
        return $this->state_id;
    }

    public function setStateId(?States $state_id): self
    {
        $this->state_id = $state_id;

        return $this;
    }

    /**
     * @return Collection|Location[]
     */
    public function getLocations(): Collection
    {
        return $this->locations;
    }

    public function addLocation(Location $location): self
    {
        if (!$this->locations->contains($location)) {
            $this->locations[] = $location;
            $location->setDistrictId($this);
        }

        return $this;
    }

    public function removeLocation(Location $location): self
    {
        if ($this->locations->contains($location)) {
            $this->locations->removeElement($location);
            // set the owning side to null (unless already changed)
            if ($location->getDistrictId() === $this) {
                $location->setDistrictId(null);
            }
        }

        return $this;
    }
    
     /**
     * Generates the magic method
     * 
     */
    public function __toString(){
        return $this->districtname;
    }
}
