<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DoctorRepository")
 */
class Doctor
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="doctor", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="doctorid_id", referencedColumnName="id", nullable=false)
     */
    private $doctorid;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $appointmentno1;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $appointmentno2;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $appointmentno3;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $degree;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $specialization;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $category;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageloc;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Location", inversedBy="doctors")
     * @ORM\JoinColumn(nullable=false)
     */
    private $location;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $fees;

    public function __construct()
    {
        $this->location = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDoctorid(): ?User
    {
        return $this->doctorid;
    }

    public function setDoctorid(User $doctorid): self
    {
        $this->doctorid = $doctorid;

        return $this;
    }

    public function getAppointmentno1(): ?string
    {
        return $this->appointmentno1;
    }

    public function setAppointmentno1(string $appointmentno1): self
    {
        $this->appointmentno1 = $appointmentno1;

        return $this;
    }

    public function getAppointmentno2(): ?string
    {
        return $this->appointmentno2;
    }

    public function setAppointmentno2(?string $appointmentno2): self
    {
        $this->appointmentno2 = $appointmentno2;

        return $this;
    }

    public function getAppointmentno3(): ?string
    {
        return $this->appointmentno3;
    }

    public function setAppointmentno3(?string $appointmentno3): self
    {
        $this->appointmentno3 = $appointmentno3;

        return $this;
    }

    public function getDegree(): ?string
    {
        return $this->degree;
    }

    public function setDegree(string $degree): self
    {
        $this->degree = $degree;

        return $this;
    }

    public function getSpecialization(): ?string
    {
        return $this->specialization;
    }

    public function setSpecialization(?string $specialization): self
    {
        $this->specialization = $specialization;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImageloc(): ?string
    {
        return $this->imageloc;
    }

    public function setImageloc(?string $imageloc): self
    {
        $this->imageloc = $imageloc;

        return $this;
    }

    /**
     * @return Collection|Location[]
     */
    public function getLocation(): Collection
    {
        return $this->location;
    }

    public function addLocation(Location $location): self
    {
        if (!$this->location->contains($location)) {
            $this->location[] = $location;
        }

        return $this;
    }

    public function removeLocation(Location $location): self
    {
        if ($this->location->contains($location)) {
            $this->location->removeElement($location);
        }

        return $this;
    }

    public function getFees(): ?string
    {
        return $this->fees;
    }

    public function setFees(?string $fees): self
    {
        $this->fees = $fees;

        return $this;
    }
    
    public function getDoctor()
    {
        $doc=$this->getDoctorid();
        return $doc;
    }
    public function getDoctorFullName()
    {
        $doc=$this->getDoctorid();
        return $doc->getFirstname().' '.$doc->getLastname();
    }
    
    public function getDoctorUserId()
    {
        $doc=$this->getDoctorid();
        return $doc->getId();
    }
    /**
     * Generates the magic method
     * 
     */
    public function __toString(){
        $doc=$this->getDoctorid();
        return $doc->getFirstname().' '.$doc->getLastname();
    }
}