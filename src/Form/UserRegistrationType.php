<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserRegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {$builder
            ->add('firstname', TextType::class)
            ->add('lastname', TextType::class)
            ->add('email', EmailType::class)
            ->add('mobileno', TextType::class)
            ->add('gender', ChoiceType::class, [
                'choices'  => [
                    'Gender' => '',
                    'Male' => 'm',
                    'Female' => 'f',
                    
                ],
            ])
            ->add('address', TextareaType::class, [ 'attr' => ['rows' => '5']])
            ->add('plainPassword', RepeatedType::class, ['type' => PasswordType::class]);
            //->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        /*$resolver->setDefaults([
            'data_class' => User::class,
        ]);*/
    }

    public function getBlockPrefix()
    {
        return 'user_registration';
    }
}
