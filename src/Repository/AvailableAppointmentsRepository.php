<?php

namespace App\Repository;

use App\Entity\AvailableAppointments;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AvailableAppointments|null find($id, $lockMode = null, $lockVersion = null)
 * @method AvailableAppointments|null findOneBy(array $criteria, array $orderBy = null)
 * @method AvailableAppointments[]    findAll()
 * @method AvailableAppointments[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AvailableAppointmentsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AvailableAppointments::class);
    }

    // /**
    //  * @return AvailableAppointments[] Returns an array of AvailableAppointments objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AvailableAppointments
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
