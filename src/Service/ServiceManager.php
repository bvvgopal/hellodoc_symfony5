<?php

namespace App\Service;

/**
 * Abstract service manager
 * Find/Create/Update/Delete Object(s)
 */
abstract class ServiceManager
{
    protected $entityManager;

    public function __construct($entityManager)
    {
        $this->entityManager    = $entityManager;
    }

    /**
     * Create a new object
     * @param string   $class
     * @return Object
     */
    public function createObject($class)
    {
        $object = new $class;
        return $object;
    }

    /**
     * flush database
     */
    public function flushEntityManager()
    {
        return $this->entityManager->flush();
    }

    /**
     * Updates an object.
     *
     * @param Object   $object
     * @param Boolean  $andFlush Whether to flush the changes (default true)
     */
    public function updateObject($object, $andFlush = true)
    {
        $this->entityManager->persist($object);
        if ($andFlush) {
            $this->flushEntityManager();
        }
        return $object;
    }

    /**
     * Delete an object
     * @param  Object $object
     * @return void
     */
    public function deleteObject($object, $andFlush = true)
    {
        $this->entityManager->remove($object);
        if ($andFlush) {
            return $this->flushEntityManager();
        }
        return true;
    }

    /**
     * Merge an object
     * @param  Object $object
     * @return Object
     */
    public function mergeObject($object)
    {
        if ($this->entityManager->contains($object)) {
            return $object;
        } else {
            $object =  $this->entityManager->merge($object);
            $this->entityManager->refresh($object);
            return $object;
        }

    }
}


