<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;

class UserManager extends ServiceManager
{    
    
    /**
     * @var String
     */
    protected $userClass;

    public function __construct(EntityManagerInterface $entityManager, $userClass)
    {
        parent::__construct($entityManager);
        $this->userClass = $userClass;
    }
    
    /**
     * User Methods
     */

    /**
     * Get User Repository
     * @return EntityRepository
     */
    protected function getUserRepository()
    {
        return $this->entityManager->getRepository($this->userClass);
    }
    
    /**
     * Create a new user
     * @return user
     */
    public function createUser()
    {
        $doctor = $this->createObject($this->userClass);
        return $user;
    }

    /**
     * Update a user information
     * @param  User $user
     * @return User
     */
    public function updateUser($user, $andFlush = true)
    {        
        return $this->updateObject($user, $andFlush);
    }

    /**
     * Delete a user
     * @param  User $user
     * @return void
     */
    public function deleteUser($user)
    {
        return $this->deleteObject($user);
    }

    /**
     * Find one user by criteria
     * @param  array $criteria
     * @return user
     */
    public function findOneUserBy(array $criteria)
    {
        return $this->getUserRepository()->findOneBy($criteria);
    }

    /**
     * Find all users by criteria
     * @param  array   $criteria
     * @param  array   $orderBy
     * @param  integer $limit
     * @param  integer $offset
     * @return user[]
     */
    public function findUsersBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->getUserRepository()->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * Get all user
     * @return Users[]
     */
    public function findUsers()
    {
        return $this->getUserRepository()->findAll();
    }
    
    
    
    public function doctorsDetails()
    {
        $users=$this->findUsers();
        foreach($users as $index => $user)
        {
            if($user->getDoctor()){
                //var_dump($user);exit;
            }
            else{
                unset($users[$index]);
            }
        }
        return $users;
    }
}