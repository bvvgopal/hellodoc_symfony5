<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\UserManager;

class HomePageController extends AbstractController
{
    /**
     * @Route("/", name="home_page")
     */
    public function index(UserManager $UserManager)
    {
        
        $doctors=$UserManager->doctorsDetails();
        
        return $this->render('home_page/index.html.twig', [
            'controller_name' => 'HomePageController', 'doctors' => $doctors, 
        ]);
    }
}
