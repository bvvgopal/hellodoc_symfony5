<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VisitsRepository")
 */
class Visits
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Doctor")
     * @ORM\JoinColumn(nullable=false)
     */
    private $doctorid;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Patient")
     * @ORM\JoinColumn(nullable=false)
     */
    private $patientid;

    /**
     * @ORM\Column(type="date")
     */
    private $visitdate;

    /**
     * @ORM\Column(type="time")
     */
    private $visittime;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $totalcost;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $amountpaid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $other_details;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDoctorid(): ?Doctor
    {
        return $this->doctorid;
    }

    public function setDoctorid(?Doctor $doctorid): self
    {
        $this->doctorid = $doctorid;

        return $this;
    }

    public function getPatientid(): ?Patient
    {
        return $this->patientid;
    }

    public function setPatientid(?Patient $patientid): self
    {
        $this->patientid = $patientid;

        return $this;
    }

    public function getVisitdate(): ?\DateTimeInterface
    {
        return $this->visitdate;
    }

    public function setVisitdate(\DateTimeInterface $visitdate): self
    {
        $this->visitdate = $visitdate;

        return $this;
    }

    public function getVisittime(): ?\DateTimeInterface
    {
        return $this->visittime;
    }

    public function setVisittime(\DateTimeInterface $visittime): self
    {
        $this->visittime = $visittime;

        return $this;
    }

    public function getTotalcost(): ?string
    {
        return $this->totalcost;
    }

    public function setTotalcost(?string $totalcost): self
    {
        $this->totalcost = $totalcost;

        return $this;
    }

    public function getAmountpaid(): ?string
    {
        return $this->amountpaid;
    }

    public function setAmountpaid(?string $amountpaid): self
    {
        $this->amountpaid = $amountpaid;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getOtherDetails(): ?string
    {
        return $this->other_details;
    }

    public function setOtherDetails(?string $other_details): self
    {
        $this->other_details = $other_details;

        return $this;
    }
}
