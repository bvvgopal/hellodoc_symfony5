<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191127104206 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE doctor_location (doctor_id INT NOT NULL, location_id INT NOT NULL, INDEX IDX_F66AFBFD87F4FB17 (doctor_id), INDEX IDX_F66AFBFD64D218E (location_id), PRIMARY KEY(doctor_id, location_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE doctor_location ADD CONSTRAINT FK_F66AFBFD87F4FB17 FOREIGN KEY (doctor_id) REFERENCES doctor (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE doctor_location ADD CONSTRAINT FK_F66AFBFD64D218E FOREIGN KEY (location_id) REFERENCES location (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE available_appointments ADD locationid_id INT NOT NULL, ADD doctorid_id INT NOT NULL, ADD amtimmings LONGTEXT DEFAULT NULL, ADD pmtimmings LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE available_appointments ADD CONSTRAINT FK_5DC7097CA2655B0C FOREIGN KEY (locationid_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE available_appointments ADD CONSTRAINT FK_5DC7097C64E0D02C FOREIGN KEY (doctorid_id) REFERENCES doctor (id)');
        $this->addSql('CREATE INDEX IDX_5DC7097CA2655B0C ON available_appointments (locationid_id)');
        $this->addSql('CREATE INDEX IDX_5DC7097C64E0D02C ON available_appointments (doctorid_id)');
        $this->addSql('ALTER TABLE district ADD state_id_id INT NOT NULL, ADD districtname VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE district ADD CONSTRAINT FK_31C15487DD71A5B FOREIGN KEY (state_id_id) REFERENCES states (id)');
        $this->addSql('CREATE INDEX IDX_31C15487DD71A5B ON district (state_id_id)');
        $this->addSql('ALTER TABLE doctor ADD doctorid_id INT NOT NULL, ADD appointmentno1 VARCHAR(20) NOT NULL, ADD appointmentno2 VARCHAR(20) DEFAULT NULL, ADD appointmentno3 VARCHAR(20) DEFAULT NULL, ADD degree VARCHAR(255) NOT NULL, ADD specialization VARCHAR(255) DEFAULT NULL, ADD category VARCHAR(255) NOT NULL, ADD description LONGTEXT DEFAULT NULL, ADD imageloc VARCHAR(255) DEFAULT NULL, ADD fees NUMERIC(10, 2) DEFAULT NULL');
        $this->addSql('ALTER TABLE doctor ADD CONSTRAINT FK_1FC0F36A64E0D02C FOREIGN KEY (doctorid_id) REFERENCES app_user (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1FC0F36A64E0D02C ON doctor (doctorid_id)');
        $this->addSql('ALTER TABLE location ADD district_id_id INT NOT NULL, ADD locationname VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE location ADD CONSTRAINT FK_5E9E89CBD0023964 FOREIGN KEY (district_id_id) REFERENCES district (id)');
        $this->addSql('CREATE INDEX IDX_5E9E89CBD0023964 ON location (district_id_id)');
        $this->addSql('ALTER TABLE patient ADD patientid_id INT NOT NULL, ADD dob DATE DEFAULT NULL, ADD age INT NOT NULL, ADD date_became_patient DATETIME NOT NULL, ADD other_details LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE patient ADD CONSTRAINT FK_1ADAD7EBABF0A384 FOREIGN KEY (patientid_id) REFERENCES app_user (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1ADAD7EBABF0A384 ON patient (patientid_id)');
        $this->addSql('ALTER TABLE app_user ADD firstname VARCHAR(255) NOT NULL, ADD lastname VARCHAR(255) DEFAULT NULL, ADD mobileno VARCHAR(20) NOT NULL, ADD gender VARCHAR(1) NOT NULL, ADD address LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE visits ADD doctorid_id INT NOT NULL, ADD patientid_id INT NOT NULL, ADD visitdate DATE NOT NULL, ADD visittime TIME NOT NULL, ADD totalcost NUMERIC(10, 2) DEFAULT NULL, ADD amountpaid NUMERIC(10, 2) DEFAULT NULL, ADD status VARCHAR(255) NOT NULL, ADD other_details LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE visits ADD CONSTRAINT FK_444839EA64E0D02C FOREIGN KEY (doctorid_id) REFERENCES doctor (id)');
        $this->addSql('ALTER TABLE visits ADD CONSTRAINT FK_444839EAABF0A384 FOREIGN KEY (patientid_id) REFERENCES patient (id)');
        $this->addSql('CREATE INDEX IDX_444839EA64E0D02C ON visits (doctorid_id)');
        $this->addSql('CREATE INDEX IDX_444839EAABF0A384 ON visits (patientid_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE doctor_location');
        $this->addSql('ALTER TABLE app_user DROP firstname, DROP lastname, DROP mobileno, DROP gender, DROP address');
        $this->addSql('ALTER TABLE available_appointments DROP FOREIGN KEY FK_5DC7097CA2655B0C');
        $this->addSql('ALTER TABLE available_appointments DROP FOREIGN KEY FK_5DC7097C64E0D02C');
        $this->addSql('DROP INDEX IDX_5DC7097CA2655B0C ON available_appointments');
        $this->addSql('DROP INDEX IDX_5DC7097C64E0D02C ON available_appointments');
        $this->addSql('ALTER TABLE available_appointments DROP locationid_id, DROP doctorid_id, DROP amtimmings, DROP pmtimmings');
        $this->addSql('ALTER TABLE district DROP FOREIGN KEY FK_31C15487DD71A5B');
        $this->addSql('DROP INDEX IDX_31C15487DD71A5B ON district');
        $this->addSql('ALTER TABLE district DROP state_id_id, DROP districtname');
        $this->addSql('ALTER TABLE doctor DROP FOREIGN KEY FK_1FC0F36A64E0D02C');
        $this->addSql('DROP INDEX UNIQ_1FC0F36A64E0D02C ON doctor');
        $this->addSql('ALTER TABLE doctor DROP doctorid_id, DROP appointmentno1, DROP appointmentno2, DROP appointmentno3, DROP degree, DROP specialization, DROP category, DROP description, DROP imageloc, DROP fees');
        $this->addSql('ALTER TABLE location DROP FOREIGN KEY FK_5E9E89CBD0023964');
        $this->addSql('DROP INDEX IDX_5E9E89CBD0023964 ON location');
        $this->addSql('ALTER TABLE location DROP district_id_id, DROP locationname');
        $this->addSql('ALTER TABLE patient DROP FOREIGN KEY FK_1ADAD7EBABF0A384');
        $this->addSql('DROP INDEX UNIQ_1ADAD7EBABF0A384 ON patient');
        $this->addSql('ALTER TABLE patient DROP patientid_id, DROP dob, DROP age, DROP date_became_patient, DROP other_details');
        $this->addSql('ALTER TABLE visits DROP FOREIGN KEY FK_444839EA64E0D02C');
        $this->addSql('ALTER TABLE visits DROP FOREIGN KEY FK_444839EAABF0A384');
        $this->addSql('DROP INDEX IDX_444839EA64E0D02C ON visits');
        $this->addSql('DROP INDEX IDX_444839EAABF0A384 ON visits');
        $this->addSql('ALTER TABLE visits DROP doctorid_id, DROP patientid_id, DROP visitdate, DROP visittime, DROP totalcost, DROP amountpaid, DROP status, DROP other_details');
    }
}
