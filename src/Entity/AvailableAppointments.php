<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AvailableAppointmentsRepository")
 */
class AvailableAppointments
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $amtimmings;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $pmtimmings;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Location")
     * @ORM\JoinColumn(nullable=false)
     */
    private $locationid;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Doctor")
     * @ORM\JoinColumn(nullable=false)
     */
    private $doctorid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmtimmings(): ?string
    {
        return $this->amtimmings;
    }

    public function setAmtimmings(?string $amtimmings): self
    {
        $this->amtimmings = $amtimmings;

        return $this;
    }

    public function getPmtimmings(): ?string
    {
        return $this->pmtimmings;
    }

    public function setPmtimmings(?string $pmtimmings): self
    {
        $this->pmtimmings = $pmtimmings;

        return $this;
    }

    public function getLocationid(): ?Location
    {
        return $this->locationid;
    }

    public function setLocationid(?Location $locationid): self
    {
        $this->locationid = $locationid;

        return $this;
    }

    public function getDoctorid(): ?Doctor
    {
        return $this->doctorid;
    }

    public function setDoctorid(?Doctor $doctorid): self
    {
        $this->doctorid = $doctorid;

        return $this;
    }
}
